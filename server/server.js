const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const app = express();
const port = 5800;

app.use(bodyParser());
app.use(cors());

app.listen(port, () => {
    console.log(`Server is live on port: ${port}`);
});

app.post('/saveResult', (req, res) => {
    const body = req.body;
    const winner = req.body.result;
    fs.readFile('db.json', (err, data) => {
        let results = JSON.parse(data);
        results[winner] = results[winner] + 1;
        results = JSON.stringify(results);

        fs.writeFile('db.json', results, (err) => {
            if (err) {
                res.send({ 'type' : 'error',  'data': 'An error has occurred' }); 
            } else {
                res.send({ 'type' : 'success', 'data' : 'Successfully saved!' });
            }
        });
    });
});