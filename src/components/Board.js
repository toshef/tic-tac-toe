import React, { Component } from 'react';
import Square from './Square';
import axios from 'axios';

class Board extends Component {

  constructor(props) {
    super(props);

    this.squares = [];
    this.state = {
        moves: 1,
        sign: true,
        board: Array(this.props.rows * this.props.cols).fill('')
    } 

    this.saveResults = this.saveResults.bind(this);
    this.checkForWinner = this.checkForWinner.bind(this);
    this.resetGame = this.resetGame.bind(this);
  }

  componentDidMount() {
    /* Create array of board items - rows & cols */
    const rows = this.props.rows;
    const cols = this.props.cols;

    for (let row = 1; row <= rows; row++) {
        this.squares[row] = [];
        for (let col = 1; col <= cols; col++) {
          this.squares[row][col] = null;
        }
    }
  }

  playerMove(id, row, col, callback) {
      let sign = this.state.sign ? "X" : "O";

      let squares = this.squares;
      if(squares[row][col] !== null) return;
      squares[row][col] = this.state.sign ? 1 : -1;

      let board = this.state.board;
      if(board[id] !== '') return;
      board[id] = sign;

      this.setState({
        moves: this.state.moves + 1,
        board: board,
        sign: !this.state.sign
      });

      callback();
  }

  saveResults(player) {
    return new Promise((resolve, reject) => {
        axios.post(`http://localhost:5800/saveResult`, { "result" : player })
            .then(() => {
                resolve();
            })
            .catch(() => {
                reject();
            });
    });
  }

  checkForWinner() {
    const moves = this.state.moves;
    const rows = this.props.rows;
    const cols = this.props.cols;
    const draw = (moves === (rows * cols));
    let winner = false;
    let player = '';
    let msg = (playerType) => { 
        winner = true;
        player = playerType;     
        alert(`Winner : ${player}`); 
        this.resetGame(winner);
    }

    /* Check if game is draw */
    if(draw) {
        this.saveResults('DRAW').then(() => msg('DRAW'));
    }

    /* Check combination for a winner & do ajax */
    var check = (sum) => {
        if(sum === rows) {
            this.saveResults('X').then(() => msg('X'));
        } else if(sum === -(rows)) {
            this.saveResults('O').then(() => msg('O'));
        }
    }

    /* Check rows */
    for(let row = 1; row <= rows; row++) {
        let sum = this.squares[row].reduce((a, b) => a + b);
        check(sum);
    }

    /* Check columns */
    let colsWinner = [];
    for(let col = 1; col <= cols; col++) {
        colsWinner[col] = [];
        for (let row = 1; row <= rows; row++) {
            colsWinner[col].push(this.squares[row][col]);
        }
    }
    
    for(let row = 1; row <= rows; row++) {
        let sum = colsWinner[row].reduce((a, b) => a + b);
        check(sum);
    }

    /* Check diagonal 1 */
    let diag1Winner = [];
    for(let row = 1; row <= rows; row++) {
        diag1Winner.push(this.squares[row][row]);
        let sum = diag1Winner.reduce((a, b) => a + b);
        check(sum);
    }

    /* Check diagonal 2 */
    let diag2Winner = [];
    let num = 1;
    for (let row = rows; row >= 1; row--) {
        diag2Winner.push(this.squares[row][num]);
        let sum = diag2Winner.reduce((a, b) => a + b);
        check(sum);        
        num++;
    }
  }

  resetGame(winner) {
    const moves = this.state.moves;
    const rows = this.props.rows;
    const cols = this.props.cols;

    if(winner || (moves === (rows * cols))) {       
        for (let row = 1; row <= rows; row++) {
            this.squares[row] = [];
            for (let col = 1; col <= cols; col++) {
              this.squares[row][col] = null;
            }
        }

        this.setState({
            moves: 1,
            sign: true,
            board: Array(this.props.rows * this.props.cols).fill('')
        })
    }
  }

  createBoard() {
    const rows = this.props.rows;
    const cols = this.props.cols;
    let table = [];
    var id = 0;

    for (let row = 1; row <= rows; row++) {
      let columns = [];

      for (let col = 1; col <= cols; col++) {
        columns.push(
                <Square 
                    key={id}
                    id={id}
                    row={row}
                    col={col}
                    sign={this.state.board[id]}
                    onClick={(id,row,col,checkForWinner) => this.playerMove(id,row,col,checkForWinner)}
                    checkForWinner={() => this.checkForWinner()}
                />
            )
        id++;
      }

      table.push(<div key={row} className="row">{columns}</div>)
    }

    return table;
  }
  
  render() {
    return (
      <div className="board">
        {this.createBoard()}
      </div>
    );
  }
}

export default Board;
