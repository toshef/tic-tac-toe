import React, { Component } from 'react';

class Square extends Component {
  
  render() {
    return (
      <div 
        className="col"
        onClick={() => { this.props.onClick(this.props.id, this.props.row, this.props.col, this.props.checkForWinner)}}
        >
        {this.props.sign}
      </div>
    );
  }
}

export default Square;
